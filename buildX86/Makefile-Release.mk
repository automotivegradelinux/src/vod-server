#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=${CROSS_COMPILE}gcc
CCC=${CROSS_COMPILE}g++
CXX=${CROSS_COMPILE}g++
FC=${CROSS_COMPILE}gfortran
AS=${CROSS_COMPILE}as

# Macros
CND_PLATFORM=GNU-Linux-x86
CND_DLIB_EXT=so
CND_CONF=Release
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
${OBJECTDIR}/Src/Console.o \
	${OBJECTDIR}/Src/IP/MostIpc.o \
	${OBJECTDIR}/Src/IP/MostMsg.o \
	${OBJECTDIR}/Src/IP/MostMsgTx.o \
	${OBJECTDIR}/Src/IP/MsgAddr.o \
	${OBJECTDIR}/Src/IP/MsgFilter.o \
	${OBJECTDIR}/Src/MacAddr.o \
	${OBJECTDIR}/Src/Main.o \
	${OBJECTDIR}/Src/ConnectionInfo.o \
	${OBJECTDIR}/Src/VodHandler.o \
	${OBJECTDIR}/Src/Multiplexer/Stream.o \
	${OBJECTDIR}/Src/Multiplexer/StreamList.o \
	${OBJECTDIR}/Src/Multiplexer/ThreadReadHdd.o \
	${OBJECTDIR}/Src/Multiplexer/ThreadWriteNetwork.o \
	${OBJECTDIR}/Src/Multiplexer/SourceFile.o \
	${OBJECTDIR}/Src/Multiplexer/SourceFileConverted.o \
	${OBJECTDIR}/Src/Multiplexer/udp-stream.o \
	${OBJECTDIR}/Src/Thread.o \

# Include Path
C_INCLUDE=-Imnsl -ISrc -ISrc/IP -ISrc/Multiplexer ${INCLUDE_PATH}

# C Compiler Flags
CFLAGS=-c -Wall -O3 -MMD -MP -DNDEBUG ${PROJECT_C_FLAGS}

# CC Compiler Flags
CCFLAGS=-Wall -O3 -MMD -MP -DNDEBUG ${PROJECT_C_FLAGS}
CXXFLAGS=${CCFLAGS}

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-lpthread

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f buildX86/Makefile-${CND_CONF}.mk VideoOnDemand

VideoOnDemand: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o VideoOnDemand ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/Src/Console.o: Src/Console.c 
	${MKDIR} -p ${OBJECTDIR}/Src
	${RM} "$@.d"
	${CC} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/Src/Console.o Src/Console.c

${OBJECTDIR}/Src/IP/MostIpc.o: Src/IP/MostIpc.cpp
	${MKDIR} -p ${OBJECTDIR}/Src/IP
	${RM} "$@.d"
	${CXX} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/Src/IP/MostIpc.o Src/IP/MostIpc.cpp

${OBJECTDIR}/Src/IP/MostMsg.o: Src/IP/MostMsg.cpp
	${MKDIR} -p ${OBJECTDIR}/Src/IP
	${RM} "$@.d"
	${CXX} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/Src/IP/MostMsg.o Src/IP/MostMsg.cpp

${OBJECTDIR}/Src/IP/MostMsgTx.o: Src/IP/MostMsgTx.cpp
	${MKDIR} -p ${OBJECTDIR}/Src/IP
	${RM} "$@.d"
	${CXX} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/Src/IP/MostMsgTx.o Src/IP/MostMsgTx.cpp

${OBJECTDIR}/Src/IP/MsgAddr.o: Src/IP/MsgAddr.cpp
	${MKDIR} -p ${OBJECTDIR}/Src/IP
	${RM} "$@.d"
	${CXX} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/Src/IP/MsgAddr.o Src/IP/MsgAddr.cpp

${OBJECTDIR}/Src/IP/MsgFilter.o: Src/IP/MsgFilter.cpp
	${MKDIR} -p ${OBJECTDIR}/Src/IP
	${RM} "$@.d"
	${CXX} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/Src/IP/MsgFilter.o Src/IP/MsgFilter.cpp

${OBJECTDIR}/Src/MacAddr.o: Src/MacAddr.cpp 
	${MKDIR} -p ${OBJECTDIR}/Src
	${RM} "$@.d"
	${CXX} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/Src/MacAddr.o Src/MacAddr.cpp

${OBJECTDIR}/Src/Main.o: Src/Main.cpp 
	${MKDIR} -p ${OBJECTDIR}/Src
	${RM} "$@.d"
	${CXX} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/Src/Main.o Src/Main.cpp

${OBJECTDIR}/Src/ConnectionInfo.o: Src/ConnectionInfo.cpp
	${MKDIR} -p ${OBJECTDIR}/Src
	${RM} "$@.d"
	${CXX} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/Src/ConnectionInfo.o Src/ConnectionInfo.cpp
		
${OBJECTDIR}/Src/VodHandler.o: Src/VodHandler.cpp
	${MKDIR} -p ${OBJECTDIR}/Src
	${RM} "$@.d"
	${CXX} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/Src/VodHandler.o Src/VodHandler.cpp

${OBJECTDIR}/Src/Multiplexer/Stream.o: Src/Multiplexer/Stream.cpp 
	${MKDIR} -p ${OBJECTDIR}/Src/Multiplexer
	${RM} "$@.d"
	${CXX} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/Src/Multiplexer/Stream.o Src/Multiplexer/Stream.cpp

${OBJECTDIR}/Src/Multiplexer/StreamList.o: Src/Multiplexer/StreamList.cpp 
	${MKDIR} -p ${OBJECTDIR}/Src/Multiplexer
	${RM} "$@.d"
	${CXX} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/Src/Multiplexer/StreamList.o Src/Multiplexer/StreamList.cpp

${OBJECTDIR}/Src/Multiplexer/ThreadReadHdd.o: Src/Multiplexer/ThreadReadHdd.cpp 
	${MKDIR} -p ${OBJECTDIR}/Src/Multiplexer
	${RM} "$@.d"
	${CXX} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/Src/Multiplexer/ThreadReadHdd.o Src/Multiplexer/ThreadReadHdd.cpp

${OBJECTDIR}/Src/Multiplexer/ThreadWriteNetwork.o: Src/Multiplexer/ThreadWriteNetwork.cpp 
	${MKDIR} -p ${OBJECTDIR}/Src/Multiplexer
	${RM} "$@.d"
	${CXX} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/Src/Multiplexer/ThreadWriteNetwork.o Src/Multiplexer/ThreadWriteNetwork.cpp

${OBJECTDIR}/Src/Multiplexer/SourceFile.o: Src/Multiplexer/SourceFile.cpp 
	${MKDIR} -p ${OBJECTDIR}/Src/Multiplexer
	${RM} "$@.d"
	${CXX} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/Src/Multiplexer/SourceFile.o Src/Multiplexer/SourceFile.cpp

${OBJECTDIR}/Src/Multiplexer/SourceFileConverted.o: Src/Multiplexer/SourceFileConverted.cpp 
	${MKDIR} -p ${OBJECTDIR}/Src/Multiplexer
	${RM} "$@.d"
	${CXX} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/Src/Multiplexer/SourceFileConverted.o Src/Multiplexer/SourceFileConverted.cpp

${OBJECTDIR}/Src/Multiplexer/udp-stream.o: Src/Multiplexer/udp-stream.c 
	${MKDIR} -p ${OBJECTDIR}/Src/Multiplexer
	${RM} "$@.d"
	${CC} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/Src/Multiplexer/udp-stream.o Src/Multiplexer/udp-stream.c

${OBJECTDIR}/Src/Thread.o: Src/Thread.cpp 
	${MKDIR} -p ${OBJECTDIR}/Src
	${RM} "$@.d"
	${CXX} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/Src/Thread.o Src/Thread.cpp


# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} VideoOnDemand

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
