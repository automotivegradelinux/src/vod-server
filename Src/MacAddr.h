/*
 * Video On Demand Samples
 *
 * Copyright (C) 2015 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

/*----------------------------------------------------------*/
/*! \file
 *  \brief  This file contains the CMacAddr class.
 */
/*----------------------------------------------------------*/
#ifndef _MACADDR_H_
#define _MACADDR_H_

#include <stdint.h>

/*----------------------------------------------------------*/
/*! \brief Class to deal with MAC addresses.
 */
/*----------------------------------------------------------*/
class CMacAddr
{
private:
    uint8_t m_Mac[6];
    char stringRep[32];

public:
    /*----------------------------------------------------------*/
    /*! \brief Default constructor of CMacAddr.
     */
    /*----------------------------------------------------------*/
    CMacAddr();



    /*----------------------------------------------------------*/
    /*! \brief Default destructor of CMacAddr.
     */
    /*----------------------------------------------------------*/
    ~CMacAddr();


    /*----------------------------------------------------------*/
    /*! \brief Constructor of CMacAddr, which uses the given bytes to initialize this class.
     *  \param pAddress - 6 Byte array containing the MAC address.
     */
    /*----------------------------------------------------------*/
    CMacAddr( const uint8_t *pAddress );

    /*----------------------------------------------------------*/
    /*! \brief Constructor of CMacAddr, which uses the given bytes to initialize this class.
     *  \param b0 - The 1st byte of the MAC address.
     *  \param b1 - The 2nd byte of the MAC address.
     *  \param b2 - The 3rd byte of the MAC address.
     *  \param b3 - The 4th byte of the MAC address.
     *  \param b4 - The 5th byte of the MAC address.
     *  \param b5 - The 6th byte of the MAC address.
     */
    /*----------------------------------------------------------*/
    CMacAddr( uint8_t b0, uint8_t b1, uint8_t b2, uint8_t b3, uint8_t b4, uint8_t b5 );



    /*----------------------------------------------------------*/
    /*! \brief Constructor of CMacAddr, which uses the given parameters to create a system wide unique MAC address.
     *  \param deviceIndex - The index of the physical device instance of the server device.
     *  \param nodeAddress - The MOST node address (e.g. 0x101).
     */
    /*----------------------------------------------------------*/
    CMacAddr( uint8_t deviceInstance, uint16_t nodeAddress );



    /*----------------------------------------------------------*/
    /*! \brief Copy-Constructor of CMacAddr, which creates a new instance out of the given object (deep copy).
     *  \param copySouce - The object, to copy.
     */
    /*----------------------------------------------------------*/
    CMacAddr( CMacAddr *copySouce );



    /*----------------------------------------------------------*/
    /*! \brief Gets a zero terminated string representation of this MAC address. The delimiter is '-'.
     *  \return A string representation of this MAC address.
     */
    /*----------------------------------------------------------*/
    const char *ToString();


    /*----------------------------------------------------------*/
    /*! \brief Gets a Byte array with the length of 6 containing the MAC address
     *  \return MAC address byte array.
     */
    /*----------------------------------------------------------*/
    const uint8_t *GetBytes();


    /*----------------------------------------------------------*/
    /*! \brief Copy the MAC address value from a given byte array.
     *  \param pAddress - 6 Byte array containing the MAC address.
     */
    /*----------------------------------------------------------*/
    void CopyValuesFromByteArray( const uint8_t *pAddress );




    /*----------------------------------------------------------*/
    /*! \brief Copy the MAC address value from a given string.
     *  \param pAddress - String containing the MAC address.
     */
    /*----------------------------------------------------------*/
    void CopyValuesFromString( const char *pAddress );


    /*----------------------------------------------------------*/
    /*! \brief Access the MAC address byte wise.
     *  \param nByte - offset of the byte array of the MAC address. Must be between 0 - 5.
     *  \return The byte value of the MAC address on the given byte offset.
     */
    /*----------------------------------------------------------*/
    uint8_t operator[]( int nByte );



    /*----------------------------------------------------------*/
    /*! \brief Compares the given CMacAddr instance with this one.
     *  \param rhs - The source CMacAddr instance, which then will be compared.
     *  \return true, if the value of the MAC address is equal. false, the value of the MAC address is different.
     */
    /*----------------------------------------------------------*/
    bool operator==( CMacAddr const &rhs );
};

#endif
