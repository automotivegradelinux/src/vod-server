/*
 * Video On Demand Samples
 *
 * Copyright (C) 2015 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

#include <stdint.h>
#include "Console.h"
#include "ThreadWriteNetwork.h"
#include "Multiplexer.h"





CThreadWriteNetwork::CThreadWriteNetwork() : 
    m_hThread(0), m_bRunning(false), m_Multiplexer(NULL)
{
}


void *CThreadWriteNetwork::Run(void *pInst) {
    assert(NULL != pInst);
    CThreadWriteNetwork *that = (CThreadWriteNetwork *)pInst;
    
    /* set thread to max priority: */
    int ret;
    pthread_t this_thread = pthread_self();
    struct sched_param params;
    params.sched_priority = sched_get_priority_max(SCHED_FIFO);
    ret = pthread_setschedparam(this_thread, SCHED_FIFO, &params);
    if (ret != 0) {
        ConsolePrintf( PRIO_ERROR, RED"CThreadWriteNetwork setschedparam(SCHED_FIFO) failed"RESETCOLOR"\n");
    }
    // Now verify the change in thread priority
    int policy = 0;
    ret = pthread_getschedparam(this_thread, &policy, &params);
    if (ret != 0) {
        ConsolePrintf( PRIO_ERROR, RED"CThreadWriteNetwork getschedparam() failed"RESETCOLOR"\n");
    }
    bool errorPrinted = false;
    WriteResult_t result;
    bool shallWait;
    while (that->m_bRunning)
    {
        shallWait = true;
        result = that->m_Multiplexer->WriteToDriver();
        switch (result)
        {
        case WriteResult_Failed:
            if (!errorPrinted)
            {
                errorPrinted = true;
                ConsolePrintf( PRIO_ERROR, RED"failed to write driver"RESETCOLOR"\n");
            }
            break;
        case WriteResult_WouldBlock:
            break;
        case WriteResult_Success:
        default:
            shallWait = false;
            break;
        }
        if ( that->m_bRunning && shallWait )
            usleep(1000);
    }
    pthread_exit(0);
}




void CThreadWriteNetwork::Start()
{
    if ( m_bRunning )
        return;
        
    if ( NULL == m_Multiplexer )
        return;
    
    m_bRunning = true;
    if ( 0 != pthread_create(&m_hThread, NULL, Run, this) )
        ConsolePrintf( PRIO_ERROR, RED"! failed to create thread"RESETCOLOR"\n");
}




void CThreadWriteNetwork::Stop()
{
    if ( m_bRunning )
    {
        m_bRunning = false;
        void *pVal;
        pthread_join(m_hThread, &pVal);
        m_hThread = 0;
    }        
}




void CThreadWriteNetwork::AddMultiplexer(CMultiplexer *multiplexer)
{
    Stop();
    m_Multiplexer = multiplexer;
    Start();
}




void CThreadWriteNetwork::RemoveMultiplexer(CMultiplexer *multiplexer)
{
    Stop();
    m_Multiplexer = NULL;
}    
