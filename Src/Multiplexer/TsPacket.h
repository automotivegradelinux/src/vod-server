/*
 * Video On Demand Samples
 *
 * Copyright (C) 2015 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

/*----------------------------------------------------------*/
/*! \file
 *  \brief  This file contains static helper C-functions to parse TS packets.
 */
/*----------------------------------------------------------*/
#ifndef TSPACKET_H
#define TSPACKET_H

//#define TS_PATTERN_CHECK

#include <stdint.h>
#include <stdbool.h>
#include <string.h>

/*----------------------------------------------------------*/
/*!
 * \brief  Handles a transport stream packet of 188 bytes
 */
/*----------------------------------------------------------*/
class CTsPacket
{
private:
    static const uint32_t ADAPTION_FIELD_CONTROL__RESERVED      = (0x00 << 4);
    static const uint32_t ADAPTION_FIELD_CONTROL__PAYLOAD_ONLY  = (0x01 << 4);
    static const uint32_t ADAPTION_FIELD_CONTROL__NO_PAYLOAD    = (0x02 << 4);
    static const uint32_t ADAPTION_FIELD_CONTROL__BOTH          = (0x03 << 4);
    static const uint32_t program_stream_map                    = 0xBC;
    static const uint32_t padding_stream                        = 0xBE;
    static const uint32_t private_stream_2                      = 0xBF;
    static const uint32_t ECM                                   = 0xF0;
    static const uint32_t EMM                                   = 0xF1;
    static const uint32_t DSMCC_stream                          = 0xF2;
    static const uint32_t H222_stream                           = 0xF8;
    static const uint32_t program_stream_directory              = 0xFF;
    static const uint32_t PROGRAM_ASSOSICIATION_SECTION         = 0x00;
    static const uint32_t PROGRAM_MAP_SECTION                   = 0x02;
    static const uint32_t ISO_IEC_11172_AUDIO                   = 0x03;
    static const uint32_t ISO_IEC_13818_2_Video                 = 0x02;
    static const uint32_t ISO_IEC_13818_3_AUDIO                 = 0x04;
    static const uint32_t ISO_IEC_13818_1_RESERVED              = 0x1B;
    static const uint8_t  PCR_FLAG                              = 0x10;
    
public:
    static const uint32_t PID_INVALID                           = 0xFFFF;
    static const uint32_t PCR_INVALID                           = 0xFFFFFFFF;
    static const uint32_t PCR_TICKS_PER_SEC                     = 45000;
    static const uint8_t  TS_PACKET_LEN                         = 188;

    

    /*----------------------------------------------------------*/
    /*! \brief extract continuity counter
     *
     *  \param pTs - uint8_t array containing TS packet
     */
    /*----------------------------------------------------------*/
    static uint32_t GetContinuityCounter(uint8_t *pTs)
    {
        return (pTs[3] & 0xF);
    }




    /*----------------------------------------------------------*/
    /*! \brief get array index where ES payload
     *
     *  \param pTs - uint8_t array containing TS packet
     */
    /*----------------------------------------------------------*/
    static uint32_t GetElementaryPayloadStart(uint8_t *pTs)
    {
        uint32_t nRet = GetPayloadStart(pTs);               // index to TS payload

        if (GetHasPesHeader(pTs)) {
            nRet += 3 + 1 + 2 + 1 + 1;                      // packet start(3) + stream_id(1) + PES_packet_length(2) + PES_scampling...(1) + PTS_DTS_flags...(1)
            nRet += (pTs[nRet] & 0xFF) + 1;                 // PES_header_data_length
        }

        return nRet;
    }




    /*----------------------------------------------------------*/
    /*! \brief check if there is a discontinuity in the stream
     *
     *  \param pTs - uint8_t array containing TS packet
     *  \param nLastDiscontinuityCounter - preceding counter
     */
    /*----------------------------------------------------------*/
    static bool GetHasDiscontinuity(uint8_t *pTs,
                                    uint32_t nLastDiscontinuityCounter)
    {
        nLastDiscontinuityCounter = (nLastDiscontinuityCounter + 1) & 0xF;
        return (pTs[3] & 0xF) != nLastDiscontinuityCounter;
    }




    /*----------------------------------------------------------*/
    /*! \brief check if a TS packet has the given PID
     *
     *  \param pTs - uint8_t array containing TS packet
     *  \param nPid - PID to check for
     *
     *  \return true if PID is equal
     */
    /*----------------------------------------------------------*/
    static bool GetHasPid(uint8_t *pTs, uint32_t nPid)
    {
        return nPid == GetPid(pTs);
    }




    /*----------------------------------------------------------*/
    /*! \brief check if a TS packet contains a PAT
     *
     *  \param pTs - uint8_t array containing TS packet
     *
     *  \return true if PAT is contained
     */
    /*----------------------------------------------------------*/
    static bool GetIsPat(uint8_t *pTs)
    {
        if (!GetHasPid(pTs, 0))
            return false;

        uint32_t nPat = GetPayloadStart(pTs);                   // start of PSI

        nPat += 1 + (0xFF & pTs[nPat]);                         // pouint32_ter_field

        return (pTs[nPat] == PROGRAM_ASSOSICIATION_SECTION);    // table_id
    }




    /*----------------------------------------------------------*/
    /*! \brief gets the PMT PID from the n-th program of a PAT
     *         packet
     *
     *  \param pTs - uint8_t array containing TS packet
     *  \param nPrg - number of program
     *
     *  \return true if PAT is contained
     */
    /*----------------------------------------------------------*/
    static uint32_t GetPmtPidFromPat(uint8_t *pTs, uint32_t nPrg)
    {
        uint32_t nPat = GetPayloadStart(pTs);                   // PSI
        nPat += 1 + (0xFF & pTs[nPat]);                         // pouint32_ter_field

        return ((0x1F & pTs[nPat + 10 + nPrg * 4]) << 8) +      // program_map_PID (high uint8_t *)
                (0xFF & pTs[nPat + 10 + nPrg * 4 + 1]);         // program_map_PID (low uint8_t *)
    }




    /*----------------------------------------------------------*/
    /*! \brief gets the audio PID from a PMT packet
     *
     *  \param pTs - uint8_t array containing TS packet
     *
     *  \return PID of 1st audio channel
     */
    /*----------------------------------------------------------*/
    static uint32_t GetAudioPidFromPmt(uint8_t *pTs)
    {
        uint32_t nPmt = GetPayloadStart(pTs);                   // PSI
        nPmt += 1 + (0xFF & pTs[nPmt]);                         // pouint32_ter_field

        if (PROGRAM_MAP_SECTION != pTs[nPmt])                   // check table_id
            return PID_INVALID;

        uint32_t section_length = ((0x0F & pTs[nPmt + 1]) << 8) +
                                  (0xFF & pTs[nPmt + 2]);
        uint32_t program_info_length = ((0x0F & pTs[nPmt + 10]) << 8) +
                                       (0xFF & pTs[nPmt + 11]);

        uint32_t n = nPmt + 12 + program_info_length;

        while (n < section_length + nPmt + 2) {                   // run through all streams
            uint32_t stream_type = pTs[n];
            uint32_t elementary_pid = ((0x1F & pTs[n + 1]) << 8) + (0xFF & pTs[n + 2]);
            uint32_t ES_info_length = ((0x0F & pTs[n + 3]) << 8) + (0xFF & pTs[n + 4]);

            if (ISO_IEC_11172_AUDIO == stream_type
                || ISO_IEC_13818_3_AUDIO == stream_type) {
                    return elementary_pid;
            }

            n += 5 + ES_info_length;                            // switch to next stream
        }

        return PID_INVALID;                                     // no audio stream found
    }




    /*----------------------------------------------------------*/
    /*! \brief gets the audio PID from a PMT packet
     *
     *  \param pTs - uint8_t array containing TS packet
     *
     *  \return PID of 1st audio channel
     */
    /*----------------------------------------------------------*/
    static uint32_t GetVideoPidFromPmt(uint8_t *pTs)
    {
        uint32_t nPmt = GetPayloadStart(pTs);               // PSI
        nPmt += 1 + (0xFF & pTs[nPmt]);                     // pouint32_ter_field

        if (PROGRAM_MAP_SECTION != pTs[nPmt])               // check table_id
            return PID_INVALID;

        uint32_t section_length = ((0x0F & pTs[nPmt + 1]) << 8) +
                                  (0xFF & pTs[nPmt + 2]);
        uint32_t program_info_length = ((0x0F & pTs[nPmt + 10]) << 8) +
                                       (0xFF & pTs[nPmt + 11]);

        uint32_t n = nPmt + 12 + program_info_length;

        while (n < section_length + nPmt + 2) {             // run through all streams
             uint32_t stream_type = pTs[n];
             uint32_t elementary_pid = ((0x1F & pTs[n + 1]) << 8) + (0xFF & pTs[n + 2]);
             uint32_t ES_info_length = ((0x0F & pTs[n + 3]) << 8) + (0xFF & pTs[n + 4]);

             if (ISO_IEC_13818_2_Video == stream_type
                 || ISO_IEC_13818_1_RESERVED == stream_type) {
                     return elementary_pid;
             }

             n += 5 + ES_info_length;                       // switch to next stream
        }

        return PID_INVALID;                                 // no audio stream found
    }




    /*----------------------------------------------------------*/
    /*! \brief checks if TS packet is the start of an I frame
     *
     *  \param pTs - uint8_t array containing TS packet
     */
    /*----------------------------------------------------------*/
    static bool GetIsStartOfIFrame(uint8_t *pTs)
    {
        uint32_t n = GetPayloadStart(pTs);

        for (; n < 188 - 4; n++)                            // search for IDR (instantaneous decoder refresh)
            if (0 == pTs[n] && 0 == pTs[n + 1] && 1 == pTs[n + 2]
                && (5 == (pTs[n + 3] & 0x1F)))
                    return true;

        return false;
    }




    /*----------------------------------------------------------*/
    /*! \brief check if TS packet contains a PES header
     *
     *  \param pTs - uint8_t array containing TS packet
     */
    /*----------------------------------------------------------*/
    static bool GetHasPesHeader(uint8_t *pTs)
    {
        if (!GetIsPayloadUnitStart(pTs))
            return false;

        uint32_t nPes = GetPayloadStart(pTs);

        return !(0                          != pTs[nPes  ] ||
                 0                          != pTs[nPes + 1] ||
                 1                          != pTs[nPes + 2] || // is it a PES Header?
                 program_stream_map         == pTs[nPes + 3] ||
                 padding_stream             == pTs[nPes + 3] ||
                 private_stream_2           == pTs[nPes + 3] ||
                 ECM                        == pTs[nPes + 3] ||
                 EMM                        == pTs[nPes + 3] ||
                 program_stream_directory   == pTs[nPes + 3] ||
                 DSMCC_stream               == pTs[nPes + 3] ||
                 H222_stream                == pTs[nPes + 3] ||
                 0                          == (0x80 & pTs[nPes + 7]));
    }




    /*----------------------------------------------------------*/
    /*! \brief check if TS packet contains a PTS
     *
     *  \param pTs - uint8_t array containing TS packet
     */
    /*----------------------------------------------------------*/
    static bool GetHasPts(uint8_t *pTs)
    {
        if (!GetHasPesHeader(pTs))
            return false;

        uint32_t nPes = GetPayloadStart(pTs);

        return (0 != ((pTs[nPes + 7] & 0x80)));             // PTS_DTS_flags is 0x2 or 0x3
    }




    /*----------------------------------------------------------*/
    /*! \brief get PCR from a packet if available
     *
     *  \param pTs - uint8_t array containing TS packet
    *
    * \return PCR or PCR_INVALID if packet contains no PCR
     */
    /*----------------------------------------------------------*/
    static uint32_t GetPcr(uint8_t *pTs)
    {
        switch ((pTs[3] & 0x30)) {                          // check if packet contains adaption field
        case ADAPTION_FIELD_CONTROL__PAYLOAD_ONLY:
        case ADAPTION_FIELD_CONTROL__RESERVED:
                return PCR_INVALID;
        default:
                break;
        }

        if (0 == pTs[4])                                    // 0 == adaption_field_length ?
                return PCR_INVALID;

        if (PCR_FLAG != (pTs[5] & PCR_FLAG))                // 1 == PCR_flag ?
                return PCR_INVALID;

        return (pTs[6] << 24) || (pTs[7] << 16) || (pTs[8] << 8) || (pTs[9]);
    }




    /*----------------------------------------------------------*/
    /*! \brief check if a TS packet has sync uint8_t *
     *
     *  \param pTs - uint8_t array containing TS packet
     */
    /*----------------------------------------------------------*/
    static bool GetHasSyncByte(uint8_t *pTs)
    {
        return 0x47 == pTs[0];
    }




    /*----------------------------------------------------------*/
    /*! \brief check if TS packet starts a new payload unit
     *
     *  \param pTs - uint8_t array containing TS packet
     */
    /*----------------------------------------------------------*/
    static bool GetIsPayloadUnitStart(uint8_t *pTs)
    {
        return (0 != (pTs[1] & 0x40));
    }




    /*----------------------------------------------------------*/
    /*! \brief get arry index of payload start in packet
     *
     *  \param pTs - uint8_t array containing TS packet
     */
    /*----------------------------------------------------------*/
    static uint32_t GetPayloadStart(uint8_t *pTs)
    {
        switch ((pTs[3] & 0x30)) {
        case ADAPTION_FIELD_CONTROL__PAYLOAD_ONLY:
                return 4;

        case ADAPTION_FIELD_CONTROL__BOTH:
                return 4 + 1 + (pTs[4] & 0xFF);

        case ADAPTION_FIELD_CONTROL__RESERVED:
        case ADAPTION_FIELD_CONTROL__NO_PAYLOAD:
                return 188;
        }

        return TS_PACKET_LEN;
    }




    /*----------------------------------------------------------*/
    /*! \brief get PES packet length from packet
     *
     *  \param pTs - uint8_t array containing TS packet
     *  \return length of PES packet in uint8_t *s
     */
    /*----------------------------------------------------------*/
    static uint32_t GetPesPacketLength(uint8_t *pTs)
    {
        uint32_t nPes = GetPayloadStart(pTs);
        return ((pTs[nPes + 4] & 0xFF) << 8) + (pTs[nPes + 5] & 0xFF);
    }




    /*----------------------------------------------------------*/
    /*! \brief extract PID from a TS packet
     *
     *  \param pTs - uint8_t array containing TS packet
     */
    /*----------------------------------------------------------*/
    static uint32_t GetPid(uint8_t *pTs)
    {
        return (((uint32_t)pTs[1] & 0x1F) << 8) + ((uint32_t)pTs[2] & 0xFF);
    }




    /*----------------------------------------------------------*/
    /*! \brief extract PTS from a TS packet in 45 kHz units
     *
     *  \param pTs - uint8_t array containing TS packet
     */
    /*----------------------------------------------------------*/
    static uint32_t GetPts(uint8_t *pTs)
    {
        uint32_t nPes = GetPayloadStart(pTs);

        return ((pTs[nPes + 9] & 0x0E) << 28) +
               ((pTs[nPes + 10] & 0xFF) << 21) +
               ((pTs[nPes + 11] & 0xFE) << 13) +
               ((pTs[nPes + 12] & 0xFF) << 6) +
               ((pTs[nPes + 13] & 0xFF) >> 2);
    }




    /*----------------------------------------------------------*/
    /*! \brief calculate CRC from a memory area
     *
     *  \param pSrc - start of memory area
     *  \param nLen - length of memory area in bytes
     */
    /*----------------------------------------------------------*/
    static uint32_t GetCrc32(uint8_t *pSrc, int nLen)
    {
        static const uint32_t CrcTable[256] = {
            0x00000000, 0x04c11db7, 0x09823b6e, 0x0d4326d9, 0x130476dc, 0x17c56b6b,
            0x1a864db2, 0x1e475005, 0x2608edb8, 0x22c9f00f, 0x2f8ad6d6, 0x2b4bcb61,
            0x350c9b64, 0x31cd86d3, 0x3c8ea00a, 0x384fbdbd, 0x4c11db70, 0x48d0c6c7,
            0x4593e01e, 0x4152fda9, 0x5f15adac, 0x5bd4b01b, 0x569796c2, 0x52568b75,
            0x6a1936c8, 0x6ed82b7f, 0x639b0da6, 0x675a1011, 0x791d4014, 0x7ddc5da3,
            0x709f7b7a, 0x745e66cd, 0x9823b6e0, 0x9ce2ab57, 0x91a18d8e, 0x95609039,
            0x8b27c03c, 0x8fe6dd8b, 0x82a5fb52, 0x8664e6e5, 0xbe2b5b58, 0xbaea46ef,
            0xb7a96036, 0xb3687d81, 0xad2f2d84, 0xa9ee3033, 0xa4ad16ea, 0xa06c0b5d,
            0xd4326d90, 0xd0f37027, 0xddb056fe, 0xd9714b49, 0xc7361b4c, 0xc3f706fb,
            0xceb42022, 0xca753d95, 0xf23a8028, 0xf6fb9d9f, 0xfbb8bb46, 0xff79a6f1,
            0xe13ef6f4, 0xe5ffeb43, 0xe8bccd9a, 0xec7dd02d, 0x34867077, 0x30476dc0,
            0x3d044b19, 0x39c556ae, 0x278206ab, 0x23431b1c, 0x2e003dc5, 0x2ac12072,
            0x128e9dcf, 0x164f8078, 0x1b0ca6a1, 0x1fcdbb16, 0x018aeb13, 0x054bf6a4,
            0x0808d07d, 0x0cc9cdca, 0x7897ab07, 0x7c56b6b0, 0x71159069, 0x75d48dde,
            0x6b93dddb, 0x6f52c06c, 0x6211e6b5, 0x66d0fb02, 0x5e9f46bf, 0x5a5e5b08,
            0x571d7dd1, 0x53dc6066, 0x4d9b3063, 0x495a2dd4, 0x44190b0d, 0x40d816ba,
            0xaca5c697, 0xa864db20, 0xa527fdf9, 0xa1e6e04e, 0xbfa1b04b, 0xbb60adfc,
            0xb6238b25, 0xb2e29692, 0x8aad2b2f, 0x8e6c3698, 0x832f1041, 0x87ee0df6,
            0x99a95df3, 0x9d684044, 0x902b669d, 0x94ea7b2a, 0xe0b41de7, 0xe4750050,
            0xe9362689, 0xedf73b3e, 0xf3b06b3b, 0xf771768c, 0xfa325055, 0xfef34de2,
            0xc6bcf05f, 0xc27dede8, 0xcf3ecb31, 0xcbffd686, 0xd5b88683, 0xd1799b34,
            0xdc3abded, 0xd8fba05a, 0x690ce0ee, 0x6dcdfd59, 0x608edb80, 0x644fc637,
            0x7a089632, 0x7ec98b85, 0x738aad5c, 0x774bb0eb, 0x4f040d56, 0x4bc510e1,
            0x46863638, 0x42472b8f, 0x5c007b8a, 0x58c1663d, 0x558240e4, 0x51435d53,
            0x251d3b9e, 0x21dc2629, 0x2c9f00f0, 0x285e1d47, 0x36194d42, 0x32d850f5,
            0x3f9b762c, 0x3b5a6b9b, 0x0315d626, 0x07d4cb91, 0x0a97ed48, 0x0e56f0ff,
            0x1011a0fa, 0x14d0bd4d, 0x19939b94, 0x1d528623, 0xf12f560e, 0xf5ee4bb9,
            0xf8ad6d60, 0xfc6c70d7, 0xe22b20d2, 0xe6ea3d65, 0xeba91bbc, 0xef68060b,
            0xd727bbb6, 0xd3e6a601, 0xdea580d8, 0xda649d6f, 0xc423cd6a, 0xc0e2d0dd,
            0xcda1f604, 0xc960ebb3, 0xbd3e8d7e, 0xb9ff90c9, 0xb4bcb610, 0xb07daba7,
            0xae3afba2, 0xaafbe615, 0xa7b8c0cc, 0xa379dd7b, 0x9b3660c6, 0x9ff77d71,
            0x92b45ba8, 0x9675461f, 0x8832161a, 0x8cf30bad, 0x81b02d74, 0x857130c3,
            0x5d8a9099, 0x594b8d2e, 0x5408abf7, 0x50c9b640, 0x4e8ee645, 0x4a4ffbf2,
            0x470cdd2b, 0x43cdc09c, 0x7b827d21, 0x7f436096, 0x7200464f, 0x76c15bf8,
            0x68860bfd, 0x6c47164a, 0x61043093, 0x65c52d24, 0x119b4be9, 0x155a565e,
            0x18197087, 0x1cd86d30, 0x029f3d35, 0x065e2082, 0x0b1d065b, 0x0fdc1bec,
            0x3793a651, 0x3352bbe6, 0x3e119d3f, 0x3ad08088, 0x2497d08d, 0x2056cd3a,
            0x2d15ebe3, 0x29d4f654, 0xc5a92679, 0xc1683bce, 0xcc2b1d17, 0xc8ea00a0,
            0xd6ad50a5, 0xd26c4d12, 0xdf2f6bcb, 0xdbee767c, 0xe3a1cbc1, 0xe760d676,
            0xea23f0af, 0xeee2ed18, 0xf0a5bd1d, 0xf464a0aa, 0xf9278673, 0xfde69bc4,
            0x89b8fd09, 0x8d79e0be, 0x803ac667, 0x84fbdbd0, 0x9abc8bd5, 0x9e7d9662,
            0x933eb0bb, 0x97ffad0c, 0xafb010b1, 0xab710d06, 0xa6322bdf, 0xa2f33668,
            0xbcb4666d, 0xb8757bda, 0xb5365d03, 0xb1f740b4
        };

        uint32_t nCrc = 0xFFFFFFFF;

        while (nLen--)
            nCrc = (nCrc << 8) ^ CrcTable[((nCrc >> 24) ^ *pSrc++) & 0xFF ];

        return nCrc;
    }




    /*----------------------------------------------------------*/
    /*! \brief calculate a PMT packet for a program containing
     *         one video and one audio stream
     *
     *  \param pTs   - pointer to TS packet to store result
     *  \param nAPid - PID of audio
     *  \param nAPid - PID of video
     *  \param nAPid - PID of PMT
     */
    /*----------------------------------------------------------*/
    static void CreatePmt(uint8_t *pTs, uint32_t nAPid, uint32_t nVPid,
                          uint32_t nPPid)
    {
        if (NULL == pTs)
            return;

        pTs[0] = 0x47;                                      // packet start

        pTs[1] = (1 << 6) |                                 // Payload Unit start indicator, PID = 0;
                 (nPPid >> 8);                              // PMT PID (HB)

        pTs[2] = nPPid & 0xFF;                              // PMT PID (LB)

        pTs[3] = (1 << 4);                                  // no adaption field

        pTs[4] = 0;                                         // PointerField: new section

        uint8_t *pPmt = pTs + 5;                            // pointer to PMT section

        pPmt[0] = 0x02;                                     // Table ID: Program Map Section

        uint16_t wSectionLength =
                12 - 3 +                                    // section until program info length
                5 +                                         // video entry
                5 +                                         // audio entry
                4;                                          // CRC

        pPmt[1] = (1 << 7) |                                // section syntax indicator
                  (3 << 4) |                                // reserved
                  (wSectionLength >> 8);                    // section syntax 1, section length
        pPmt[2] = wSectionLength & 0xFF;

        pPmt[3] = (nPPid >> 8);                             // program number (HB)
        pPmt[4] = nPPid & 0xFF;                             // program number (LB)

        pPmt[5] = (3 << 6) | 1;                             // version, current/next

        pPmt[6] = 0;                                        // section number
        pPmt[7] = 0;                                        // last section number

        pPmt[8] = (7 << 5)      |                           // reserved
                  (nVPid >> 8);                             // PCR PID, assuming video has PCR

        pPmt[9] = nVPid & 0xFF;                             // PCR PID (LB)

        pPmt[10] = (0xF << 4) |                             // reserved
                   (0);                                     // program info length

        pPmt[11] = 0;

        // video
        pPmt[12] = 27;                                      // stream type = video

        pPmt[13] = (7 << 5)      |                          // reserved
                   (nVPid >> 8);                            // ES PID (HB)
        pPmt[14] = nVPid & 0xFF;                            // ES PID (LB)

        pPmt[15] = (0xF << 4) |                             // reserved
                   (0);                                     // ES Info length
        pPmt[16] = 0;                                       // ES Info length

        // audio
        pPmt[17] = 3;                                       // stream type = audio

        pPmt[18] = (7 << 5)      |                          // reserved
                   (nAPid >> 8);                            // ES PID

        pPmt[19] = nAPid & 0xFF;

        pPmt[20] = (0xF << 4) |                             // reserved
                   (0);                                     // ES Info

        pPmt[21] = 0;

        // CRC
        uint32_t nCrc = GetCrc32(pPmt,
                                 wSectionLength + 3 - 4);// Crc for entire pmt section
        pPmt[wSectionLength + 3 - 4 + 0] = (nCrc >> 24) & 0xFF;
        pPmt[wSectionLength + 3 - 4 + 1] = (nCrc >> 16) & 0xFF;
        pPmt[wSectionLength + 3 - 4 + 2] = (nCrc >>  8) & 0xFF;
        pPmt[wSectionLength + 3 - 4 + 3] = (nCrc) & 0xFF;
    }
        
        
        
    /*----------------------------------------------------------*/
    /*! \brief calculate a stuffing packet
     *
     *  \param pTs   - pointer to TS packet to store result
     *  \param nPatternCount - Actual pattern packet count, shall be incremented each time.
     */
    /*----------------------------------------------------------*/
    static void CreateStuffing(uint8_t *pTs, uint32_t nPatternCount)
    {
        pTs[0] = 0x47;
        pTs[1] = 0x1F;
        pTs[2] = 0xFF;
        pTs[3] = 0x00;
#ifdef TS_PATTERN_CHECK
        memcpy(&pTs[4], &nPatternCount, sizeof(nPatternCount));
    uint8_t startByte = (uint8_t)(nPatternCount & 0xFF);
    for (uint8_t i = 8; i < 188; i++) {
            pTs[i] = startByte + i;
        }
#endif
    }

};

#endif //TSPACKET_H
