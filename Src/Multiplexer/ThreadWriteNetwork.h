/*
 * Video On Demand Samples
 *
 * Copyright (C) 2015 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

/*----------------------------------------------------------*/
/*! \file
 *  \brief  This file contains the CThreadWriteNetwork class.
 */
/*----------------------------------------------------------*/
#ifndef THREADWRITENETWORK_H
#define	THREADWRITENETWORK_H

#include "SafeVector.h"
#include <pthread.h>

class CMultiplexer;




/*----------------------------------------------------------*/
/*! \brief class creating a thread to call stream's sending
 *        function.
 */
/*----------------------------------------------------------*/
class CThreadWriteNetwork
{
    pthread_t m_hThread;
    bool m_bRunning;
    CMultiplexer *m_Multiplexer;
    
    /*----------------------------------------------------------*/
    /*! \brief thread reading from all stream lists
     */
    /*----------------------------------------------------------*/
    static void *Run(void *pInst);
    
public:    
    CThreadWriteNetwork();
    
    
    /*----------------------------------------------------------*/
    /*! \brief start reading thread
     */
    /*----------------------------------------------------------*/
    void Start();
    
    
    
    
    /*----------------------------------------------------------*/
    /*! \brief stop reading thread
     */
    /*----------------------------------------------------------*/
    void Stop();



    
public:
    /*----------------------------------------------------------*/
    /*! \brief add a multiplexer to send thread
     *  \param multiplexer - multiplexer to add to sending thread
     */
    /*----------------------------------------------------------*/
    void AddMultiplexer(CMultiplexer *multiplexer);
    
    
    
    
    /*----------------------------------------------------------*/
    /*! \brief remove a multiplexer from send thread
     */
    /*----------------------------------------------------------*/
    void RemoveMultiplexer(CMultiplexer *multiplexer);
};

#endif
